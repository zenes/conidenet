-- MariaDB dump 10.19  Distrib 10.5.9-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: conidenet
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app`
--

DROP TABLE IF EXISTS `app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auth` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `key_UNIQUE` (`auth`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app`
--

LOCK TABLES `app` WRITE;
/*!40000 ALTER TABLE `app` DISABLE KEYS */;
INSERT INTO `app` VALUES (1,'c237b3ef57a19bcdeeab4923df6ae9db1734','88Iai0JsbB+FnXnBHfXrN3ACMJv5eGn3H5Hov3SrXq4=');
/*!40000 ALTER TABLE `app` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branch`
--

DROP TABLE IF EXISTS `branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branch` (
  `id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `country_id` int(2) NOT NULL,
  `name` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Cidenet',
  `domain` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`,`country_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `domain_UNIQUE` (`domain`),
  UNIQUE KEY `url_UNIQUE` (`url`),
  KEY `fk_branch_country1_idx` (`country_id`),
  CONSTRAINT `fk_branch_country1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branch`
--

LOCK TABLES `branch` WRITE;
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
INSERT INTO `branch` VALUES (1,1,'Cidenet Col','cidenet.com.co','cidenet.com.co'),(2,2,'Cidenet EEUU','cidenet.com.us','cidenet.com.us'),(34,1,'Cidesset CU','d.com.ca','https://cidenet.com.ca');
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Colombia'),(2,'Estados Unidos'),(10,'Macedonia'),(11,'Venezuela'),(12,'Panama'),(13,'Surinam'),(14,'Egipto'),(15,'España'),(16,'Rusia'),(17,'Australia'),(18,'Madagascar'),(19,'Sudafrica');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `branch_id` tinyint(1) NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_department_branch1_idx` (`branch_id`),
  CONSTRAINT `fk_department_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,1,'Administracion','Administrativos'),(2,1,'Financiera',NULL),(3,1,'Tecnico',NULL),(4,1,'Desarrollo',NULL),(5,1,'Servicios',NULL),(6,2,'Administracion',NULL),(7,2,'Financiera',NULL),(8,2,'Tecnico',NULL),(9,2,'Desarrollo',NULL),(10,2,'Servicios',NULL),(22,34,'Sectretaria',NULL);
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `editlog`
--

DROP TABLE IF EXISTS `editlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `editlog` (
  `id` int(127) NOT NULL AUTO_INCREMENT,
  `editor_id` int(32) NOT NULL COMMENT 'employee in charge of the edition of the user',
  `user_id` int(32) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `editlog`
--

LOCK TABLES `editlog` WRITE;
/*!40000 ALTER TABLE `editlog` DISABLE KEYS */;
INSERT INTO `editlog` VALUES (7,13,13,'2021-04-02 16:42:12'),(8,13,13,'2021-04-02 16:43:58'),(9,13,13,'2021-04-02 16:46:45'),(10,13,17,'2021-04-03 02:05:12'),(11,13,18,'2021-04-03 02:05:45'),(12,13,19,'2021-04-03 02:06:25'),(13,13,13,'2021-04-03 09:08:15'),(14,13,13,'2021-04-03 09:08:48'),(15,13,13,'2021-04-03 09:13:22'),(16,13,13,'2021-04-03 09:14:12'),(17,13,13,'2021-04-03 09:14:39'),(18,13,13,'2021-04-03 09:15:30'),(19,13,13,'2021-04-03 09:15:40'),(20,13,13,'2021-04-03 09:20:42'),(21,13,13,'2021-04-03 09:22:52'),(22,13,13,'2021-04-03 09:25:12'),(23,13,13,'2021-04-03 09:26:45'),(24,13,13,'2021-04-03 09:27:22'),(25,13,13,'2021-04-03 09:27:35'),(26,13,13,'2021-04-03 09:27:43'),(27,13,13,'2021-04-03 09:30:57'),(28,13,13,'2021-04-03 09:31:36'),(29,13,13,'2021-04-03 09:33:09'),(30,13,13,'2021-04-03 09:35:00'),(31,13,13,'2021-04-03 09:38:05'),(32,13,13,'2021-04-03 09:38:24'),(33,13,13,'2021-04-03 09:38:27'),(34,13,13,'2021-04-03 13:37:31'),(35,13,13,'2021-04-03 14:13:04');
/*!40000 ALTER TABLE `editlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gatelog`
--

DROP TABLE IF EXISTS `gatelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gatelog` (
  `id` int(127) NOT NULL AUTO_INCREMENT,
  `user_id` int(32) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `type` enum('IN','OUT') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gatelog`
--

LOCK TABLES `gatelog` WRITE;
/*!40000 ALTER TABLE `gatelog` DISABLE KEYS */;
INSERT INTO `gatelog` VALUES (1,13,'2021-04-02 16:59:04','IN'),(2,13,'2021-04-02 17:14:03','OUT'),(3,13,'2021-04-02 17:17:10','IN'),(4,13,'2021-04-03 02:45:59','IN');
/*!40000 ALTER TABLE `gatelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gender`
--

DROP TABLE IF EXISTS `gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gender` (
  `id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('Non_binary','Binary') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gender`
--

LOCK TABLES `gender` WRITE;
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` VALUES (1,'Hombre','Binary'),(2,'Mujer','Binary'),(3,'Hombre','Non_binary'),(4,'Mujer','Non_binary');
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idtype`
--

DROP TABLE IF EXISTS `idtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `idtype` (
  `id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idtype`
--

LOCK TABLES `idtype` WRITE;
/*!40000 ALTER TABLE `idtype` DISABLE KEYS */;
INSERT INTO `idtype` VALUES (1,'Cédula'),(3,'Extrangeria'),(6,'NIT'),(2,'Pasaporte');
/*!40000 ALTER TABLE `idtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `idtype_id` tinyint(1) NOT NULL,
  `department_id` int(2) NOT NULL,
  `gender_id` tinyint(1) NOT NULL,
  `employeeid` varchar(63) COLLATE utf8mb4_unicode_ci NOT NULL,
  `documentid` varchar(19) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(19) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secondname` varchar(49) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(19) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(19) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date DEFAULT NULL,
  `startdate` date NOT NULL,
  `regdate` datetime NOT NULL DEFAULT current_timestamp(),
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  UNIQUE KEY `numeroid_UNIQUE` (`documentid`),
  UNIQUE KEY `idcode_UNIQUE` (`employeeid`),
  UNIQUE KEY `password_UNIQUE` (`password`),
  UNIQUE KEY `email_UNIQUE` (`email`) USING HASH,
  KEY `fk_user_department1_idx` (`department_id`),
  KEY `fk_user_gender1_idx` (`gender_id`),
  KEY `fk_user_idtype1_idx` (`idtype_id`),
  CONSTRAINT `fk_user_department1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_gender1` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_idtype1` FOREIGN KEY (`idtype_id`) REFERENCES `idtype` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (13,1,1,4,'910ddbae281e65ccd73fdfbc3ed31a84d22b2e0e','44444444','$2a$04$sXlKt/JOuA6wnXiUsX0iCuOrCnqvy97SiF1/0lh4vibj1EtWNeGci','juan.romero@cidenet.com.co','Juan','Caceres','Romero','Buitron','1990-10-10','2020-11-25','2021-04-02 14:15:22','ACTIVE'),(14,1,4,1,'116b0a28eaefa6fdff01788b0e83930825e5325b','8181723123','$2a$04$JsCYpiHNUx9Qe3KhHMq.Zu.cgoYmDtOgLNwuoNoN/.Y0hCwLyg5Ci','juanito.delascasas.1@cidenet.com.co','Juanito','ssss','De las casas','Buitron','1990-10-10','2020-11-25','2021-04-02 14:15:36','ACTIVE'),(19,1,1,2,'6819c1129edcdfa3fbb5332d04017de3898a310f','33333333','$2a$04$PVAG.zSMBRH0rGQ7sx49wOwmnaPyezJclfFamOpeVczurQRiQp3vi','juanito.delascasas.2@cidenet.com.co','Juanito','f','De las casas','Buitron','1990-10-10','2020-11-25','2021-04-03 02:06:25','ACTIVE'),(24,1,3,3,'0b4fd12154394335835fd35d3e2a26a0060240a3','1787238','$2a$04$.nsqvnmIgTJxV5O3PkP4yO5Lo32dw15Ze1HoX3b.dE.w3H0tJNte6','andres.delascasas@cidenet.com.co','Andres','aaa','De las casas','Buitron','1990-10-10','2020-11-25','2021-04-03 02:45:59','ACTIVE'),(29,1,2,2,'b9315ddfbe803a7708cb5565eda5bda3af957d40','1231231','$2a$04$L6EM9LsR9FepQpNH0C8h4eims/LegkNPWxRrGavTL/Rk2ye/xpaUy','qweqw.asdasd@cidenet.com.co','qweqw',NULL,'asdasd','asdasda',NULL,'2021-04-01','2021-04-04 23:52:36','ACTIVE');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-05 13:08:12

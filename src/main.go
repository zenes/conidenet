package main

/*
  This is the main package of the app
  responsible to run the entire server.

  I will use mux as a router this time
  but it can be replaced by the http server
  included by default in Go.
*/

import (
	"log"
	"net/http"
)

/*
	This function have a list with all the routes supported
	by the application
*/
func main() {
	router := NewRouter()
	server := http.ListenAndServe(":9000", router)
	log.Fatal(server)
}

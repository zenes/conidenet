package main

/*
	This file contains all the routes for the application,
	if you want to have a new route, please add it here
*/

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/zenes/conidenet/src/routes"
)

type path struct {
	name       string
	method     string
	pattern    string
	handleFunc http.HandlerFunc
}

type paths []path

/*
	This is the routher with all the paths of the app

	Add a new url path here
*/
var urlPaths = paths{
	path{
		"Index",
		"GET",
		"/",
		routes.Index,
	},
	path{
		"Login",
		"POST",
		"/login",
		routes.Login,
	},
	path{
		"QRLogin",
		"POST",
		"/qrinout",
		routes.QrInOut,
	},
	// BRANCH
	path{
		"Branch",
		"GET",
		"/branch/{id}",
		routes.GetBranch,
	},
	path{
		"Branches",
		"GET",
		"/branches",
		routes.GetBranches,
	},
	path{
		"Branch",
		"DELETE",
		"/branch/{id}",
		routes.DeleteBranch,
	},
	path{
		"Branch",
		"PUT",
		"/branch",
		routes.NewBranch,
	},
	path{
		"Branch",
		"POST",
		"/branch",
		routes.UpdateBranch,
	},
	// END BRANCH
	// COUNTRY
	path{
		"Country",
		"GET",
		"/country/{id}",
		routes.GetCountry,
	},
	path{
		"Countries",
		"GET",
		"/countries",
		routes.GetCountries,
	},
	path{
		"Country",
		"DELETE",
		"/country/{id}",
		routes.DeleteCountry,
	},
	path{
		"Country",
		"PUT",
		"/country",
		routes.NewCountry,
	},
	path{
		"Country",
		"POST",
		"/country",
		routes.UpdateCountry,
	},
	// END COUNTRY
	// DEPARTMENT
	path{
		"Department",
		"GET",
		"/department/{id}",
		routes.GetDepartment,
	},
	path{
		"Departments",
		"GET",
		"/departments",
		routes.GetDepartments,
	},
	path{
		"Department",
		"DELETE",
		"/department/{id}",
		routes.DeleteDepartment,
	},
	path{
		"Department",
		"PUT",
		"/department",
		routes.NewDepartment,
	},
	path{
		"Department",
		"POST",
		"/department",
		routes.UpdateDepartment,
	},
	// END DEPARTMENT
	// EDITLOG
	path{
		"EditLogs",
		"GET",
		"/editlogs",
		routes.GetEditLogs,
	},
	path{
		"EditLog",
		"GET",
		"/editlog/{id}",
		routes.GetEditLog,
	},
	// END EDITLOG
	// GATELOG
	path{
		"GateLogs",
		"GET",
		"/gatelogs",
		routes.GetGateLogs,
	},
	path{
		"Gatelog",
		"GET",
		"/gatelog/{id}",
		routes.GetGateLog,
	},
	// END GATELOG
	// GENDER
	path{
		"Gender",
		"GET",
		"/gender/{id}",
		routes.GetGender,
	},
	path{
		"Genderes",
		"GET",
		"/genders",
		routes.GetGenderes,
	},
	path{
		"Gender",
		"DELETE",
		"/gender/{id}",
		routes.DeleteGender,
	},
	path{
		"Gender",
		"PUT",
		"/gender",
		routes.NewGender,
	},
	path{
		"Gender",
		"POST",
		"/gender",
		routes.UpdateGender,
	},
	// END GENDER
	// IDTYPE
	path{
		"IdType",
		"GET",
		"/idtype/{id}",
		routes.GetIdType,
	},
	path{
		"IdTypees",
		"GET",
		"/idtypes",
		routes.GetIdTypees,
	},
	path{
		"IdType",
		"DELETE",
		"/idtype/{id}",
		routes.DeleteIdType,
	},
	path{
		"IdType",
		"PUT",
		"/idtype",
		routes.NewIdType,
	},
	path{
		"IdType",
		"POST",
		"/idtype",
		routes.UpdateIdType,
	},
	// USER
	path{
		"User",
		"GET",
		"/user/{id}",
		routes.GetUser,
	},
	path{
		"Users",
		"GET",
		"/users",
		routes.GetUsers,
	},
	path{
		"User",
		"DELETE",
		"/user/{id}",
		routes.DeleteUser,
	},
	path{
		"User",
		"PUT",
		"/user",
		routes.NewUser,
	},
	path{
		"User",
		"POST",
		"/user/{id}",
		routes.UpdateUser,
	},
	path{
		"User",
		"POST",
		"/changepassword/{id}",
		routes.UpdatePassword,
	},
	path{
		"User",
		"POST",
		"/changestatus/{id}",
		routes.UpdateStatus,
	},
	// END IDTYPE
}

/*
	This function is the responsible of creating the mux
	router with all the routes included in this file
	don't modify it
*/
func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, path := range urlPaths {
		router.Methods(path.method).
			Path(path.pattern).
			Name(path.name).
			Handler(path.handleFunc)
	}
	return router
}

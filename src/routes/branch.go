package routes

/*
	This package contains functions needed
	in the index of the application.
*/
import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	@w is the variable needed for the return of data,
	@r is just needed to return the http response
*/

/*
	This function returns all the information of a branch
	by id
*/
func GetBranch(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	branchId, _ := strconv.Atoi(params["id"])

	branch := mod.Branch{}
	branch.Id = branchId
	err := branch.GetById()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(branch)
	response(w, 200, string(result))
}

/*
	This function returns all the branches
*/
func GetBranches(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	branch := mod.Branch{}
	branches, err := branch.GetAll()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(branches)
	response(w, 200, string(result))

}

/*
	With this function you can create a new branch into the database
*/
func NewBranch(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	decoder := json.NewDecoder(r.Body)
	branch := mod.Branch{}

	_ = decoder.Decode(&branch)
	branch.Id = branch.Add()
	if branch.Id != 0 {

		branch.Country.Id = branch.Country_id
		branch.Country.GetById()
		result, _ := json.Marshal(branch)
		response(w, 201, string(result))
		return
	}
	response(w, 400, "Error")
}

/*
	With this function you can delete a branch from the database
*/
func DeleteBranch(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	branchId, _ := strconv.Atoi(params["id"])

	branch := mod.Branch{}
	branch.Id = branchId
	if branch.Delete() {
		response(w, 200, "DELETED")
		r.Body.Close()
		return
	}
	response(w, 400, "NOT DELETED")
}

/*
	With this function you can delete a branch from the database
*/
func UpdateBranch(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	decoder := json.NewDecoder(r.Body)
	branch := mod.Branch{}

	_ = decoder.Decode(&branch)

	if branch.Update() {
		branch.Country.Id = branch.Country_id
		branch.Country.GetById()
		result, _ := json.Marshal(branch)
		response(w, 200, string(result))
		return
	}
	response(w, 400, "ERROR")
}

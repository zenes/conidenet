package routes

/*
	This package contains functions needed
	in the index of the application.
*/
import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	@w is the variable needed for the return of data,
	@r is just needed to return the http response
*/

/*
	This function returns all the information of a country
	by id
*/
func GetCountry(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	countryId, _ := strconv.Atoi(params["id"])

	country := mod.Country{}
	country.Id = countryId
	err := country.GetById()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(country)
	response(w, 200, string(result))
}

/*
	This function returns all the countryes
*/
func GetCountries(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	country := mod.Country{}
	countries, err := country.GetAll()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(countries)
	response(w, 200, string(result))

}

/*
	With this function you can create a new country into the database
*/
func NewCountry(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	decoder := json.NewDecoder(r.Body)
	country := mod.Country{}

	_ = decoder.Decode(&country)
	country.Id = country.Add()
	if country.Id != 0 {
		result, _ := json.Marshal(country)
		response(w, 201, string(result))
		return
	}
	response(w, 400, "Error")
}

/*
	With this function you can delete a country from the database
*/
func DeleteCountry(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	countryId, _ := strconv.Atoi(params["id"])

	country := mod.Country{}
	country.Id = countryId
	if country.Delete() {
		response(w, 200, "DELETED")
		r.Body.Close()
		return
	}
	response(w, 400, "NOT DELETED")
}

/*
	With this function you can delete a country from the database
*/
func UpdateCountry(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	decoder := json.NewDecoder(r.Body)
	country := mod.Country{}

	_ = decoder.Decode(&country)

	if country.Update() {
		result, _ := json.Marshal(country)
		response(w, 200, string(result))
		return
	}
	response(w, 400, "ERROR")
}

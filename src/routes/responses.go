package routes

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/zenes/conidenet/src/models"
	"golang.org/x/crypto/bcrypt"
)

/*
	This function sets the json response headers and the
	status code for the requests
*/
func response(w http.ResponseWriter, status int, result string) {

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	if status != 200 && status != 201 {
		code := strconv.Itoa(status)
		fmt.Fprint(w, "{\"status\":"+code+",\"message\": \""+result+"\"}")
		return
	}
	fmt.Fprint(w, result)
}

/*
	This func verifies the auth token from the app
	The function requires that the app calling send a bearer token
	with a prefix in which the prefix is the app identifier and
	the token is an authorized token.
*/

func authorize(w http.ResponseWriter, r *http.Request) bool {
	app := models.App{}
	/*
		This is maybe not the best solution for the authorization of the app
		but i had to little time to implement other type of solutions like
		JWT
	*/
	reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, " ")

	if len(splitToken) < 2 {
		response(w, 401, "UNAUTHORIZED")
		return false
	}

	app.Name = splitToken[0]
	app.Id = 1
	err := app.GetByName()

	err = bcrypt.CompareHashAndPassword([]byte(splitToken[1]), []byte(app.Auth))
	if err != nil {
		log.Print(err)
		response(w, 401, "UNAUTHORIZED")
		return false
	}
	return true
}

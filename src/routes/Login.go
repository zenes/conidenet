package routes

/*
	This package contains functions needed
	in the index of the application.
*/
import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	mod "gitlab.com/zenes/conidenet/src/models"
	"golang.org/x/crypto/bcrypt"
)

/*
	This function is used for the login page
	@w is the variable needed for the return of data,
	@r is just needed to return the http response
*/
func Login(w http.ResponseWriter, r *http.Request) {
	reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, " ")

	decoder := json.NewDecoder(r.Body)
	usr := mod.User{}

	_ = decoder.Decode(&usr)

	if usr.Login(usr.Password) {
		app := mod.App{}
		app.Name = splitToken[0]
		err := app.GetByName()

		type resp struct {
			Id         int    `json:"id"`
			Email      string `json:"email"`
			Documentid string `json:"document_id"`
			Name       string `json:"name"`
			Token      string `json:"token"`
		}

		if err != nil {
			log.Print(err)
			response(w, 401, "UNAUTHORIZED")
			return
		}

		token, _ := bcrypt.GenerateFromPassword([]byte(app.Auth), bcrypt.MinCost)
		res := resp{}
		res.Id = usr.Id
		res.Email = usr.Email
		res.Documentid = usr.Documentid
		res.Name = usr.Name
		res.Token = string(token)

		resJson, _ := json.Marshal(res)

		response(w, 200, string(resJson))
		return
	}

	response(w, 401, "UNAUTHORIZED")
}

func QrInOut(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Login con QR")
}

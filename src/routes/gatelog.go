package routes

/*
	This package contains functions needed
	in the index of the application.
*/
import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	@w is the variable needed for the return of data,
	@r is just needed to return the http response
*/

/*
	This function returns all the information of a gatelog
	by id
*/
func GetGateLog(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	gatelogId, _ := strconv.Atoi(params["id"])

	gatelog := mod.GateLog{}
	gatelog.Id = gatelogId
	err := gatelog.GetById()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(gatelog)
	response(w, 200, string(result))
}

/*
	This function returns all the gateloges
*/
func GetGateLogs(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	gatelog := mod.GateLog{}
	gatelogs, err := gatelog.GetAll()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(gatelogs)
	response(w, 200, string(result))

}

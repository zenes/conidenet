package routes

/*
	This package contains functions needed
	in the index of the application.
*/
import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	@w is the variable needed for the return of data,
	@r is just needed to return the http response
*/

/*
	This function returns all the information of a department
	by id
*/
func GetDepartment(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	departmentId, _ := strconv.Atoi(params["id"])

	department := mod.Department{}
	department.Id = departmentId
	err := department.GetById()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(department)
	response(w, 200, string(result))
}

/*
	This function returns all the departmentes
*/
func GetDepartments(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	department := mod.Department{}
	departments, err := department.GetAll()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(departments)
	response(w, 200, string(result))

}

/*
	With this function you can create a new department into the database
*/
func NewDepartment(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	decoder := json.NewDecoder(r.Body)
	department := mod.Department{}

	_ = decoder.Decode(&department)
	department.Id = department.Add()
	if department.Id != 0 {
		department.Branch.Id = department.Branch_id
		department.Branch.GetById()
		result, _ := json.Marshal(department)
		response(w, 201, string(result))
		return
	}
	response(w, 400, "Error")
}

/*
	With this function you can delete a department from the database
*/
func DeleteDepartment(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	departmentId, _ := strconv.Atoi(params["id"])

	department := mod.Department{}
	department.Id = departmentId
	if department.Delete() {
		response(w, 200, "DELETED")
		r.Body.Close()
		return
	}
	response(w, 400, "NOT DELETED")
}

/*
	With this function you can delete a department from the database
*/
func UpdateDepartment(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	decoder := json.NewDecoder(r.Body)
	department := mod.Department{}

	_ = decoder.Decode(&department)

	if department.Update() {
		result, _ := json.Marshal(department)
		response(w, 200, string(result))
		return
	}
	response(w, 400, "ERROR")
}

package routes

/*
	This package contains functions needed
	in the index of the application.
*/
import (
	"net/http"
)

/*
	@w is the variable needed for the return of data,
	@r is just needed to return the http response
*/
func Index(w http.ResponseWriter, r *http.Request) {
	response(w, 404, "")
}

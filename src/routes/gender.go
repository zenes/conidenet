package routes

/*
	This package contains functions needed
	in the index of the application.
*/
import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	@w is the variable needed for the return of data,
	@r is just needed to return the http response
*/

/*
	This function returns all the information of a gender
	by id
*/
func GetGender(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	genderId, _ := strconv.Atoi(params["id"])

	gender := mod.Gender{}
	gender.Id = genderId
	err := gender.GetById()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(gender)
	response(w, 200, string(result))
}

/*
	This function returns all the genderes
*/
func GetGenderes(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	gender := mod.Gender{}
	genderes, err := gender.GetAll()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(genderes)
	response(w, 200, string(result))

}

/*
	With this function you can create a new gender into the database
*/
func NewGender(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	decoder := json.NewDecoder(r.Body)
	gender := mod.Gender{}

	_ = decoder.Decode(&gender)
	gender.Id = gender.Add()
	if gender.Id != 0 {
		result, _ := json.Marshal(gender)
		response(w, 201, string(result))
		return
	}
	response(w, 400, "Error")
}

/*
	With this function you can delete a gender from the database
*/
func DeleteGender(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	genderId, _ := strconv.Atoi(params["id"])

	gender := mod.Gender{}
	gender.Id = genderId
	if gender.Delete() {
		response(w, 200, "DELETED")
		r.Body.Close()
		return
	}
	response(w, 400, "NOT DELETED")
}

/*
	With this function you can delete a gender from the database
*/
func UpdateGender(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	decoder := json.NewDecoder(r.Body)
	gender := mod.Gender{}

	_ = decoder.Decode(&gender)

	if gender.Update() {
		result, _ := json.Marshal(gender)
		response(w, 200, string(result))
		return
	}
	response(w, 400, "ERROR")
}

package routes

/*
	This package contains functions needed
	in the index of the application.
*/
import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	@w is the variable needed for the return of data,
	@r is just needed to return the http response
*/

/*
	This function returns all the information of a editlog
	by id
*/
func GetEditLog(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	editlogId, _ := strconv.Atoi(params["id"])

	editlog := mod.EditLog{}
	editlog.Id = editlogId
	err := editlog.GetById()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(editlog)
	response(w, 200, string(result))
}

/*
	This function returns all the editloges
*/
func GetEditLogs(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	editlog := mod.EditLog{}
	countries, err := editlog.GetAll()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(countries)
	response(w, 200, string(result))

}

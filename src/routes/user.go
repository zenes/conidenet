package routes

/*
	This package contains functions needed
	in the index of the application.
*/
import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	@w is the variable needed for the return of data,
	@r is just needed to return the http response
*/

/*
	This function returns all the information of a user
	by id
*/
func GetUser(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	userId, _ := strconv.Atoi(params["id"])

	user := mod.User{}
	user.Id = userId
	err := user.GetById()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(user)
	response(w, 200, string(result))
}

/*
	This function returns all the useres
*/
func GetUsers(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	user := mod.User{}
	users, err := user.GetAll()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(users)
	response(w, 200, string(result))

}

/*
	With this function you can create a new user into the database
*/
func NewUser(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	decoder := json.NewDecoder(r.Body)
	user := mod.User{}

	_ = decoder.Decode(&user)
	user.Id = user.Add()
	if user.Id != 0 {
		result, _ := json.Marshal(user)
		response(w, 201, string(result))
		return
	}
	response(w, 400, "Error")
}

/*
	With this function you can delete a user from the database
*/
func DeleteUser(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	userId, _ := strconv.Atoi(params["id"])

	user := mod.User{}
	user.Id = userId
	if user.Delete() {
		response(w, 200, "DELETED")
		r.Body.Close()
		return
	}
	response(w, 400, "NOT DELETED")
}

/*
	With this function you can update a user from the database
*/
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	editorId, _ := strconv.Atoi(params["id"])

	decoder := json.NewDecoder(r.Body)
	user := mod.User{}

	_ = decoder.Decode(&user)

	if user.Update(editorId) {
		result, _ := json.Marshal(user)
		response(w, 200, string(result))
		return
	}
	response(w, 400, "ERROR")
}

/*
	With this function you can change the password of a user from the database
*/
func UpdatePassword(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	type passwords struct {
		Id      int    `json:"id"`
		NewPass string `json:"new_password"`
	}
	params := mux.Vars(r)
	editorId, _ := strconv.Atoi(params["id"])

	decoder := json.NewDecoder(r.Body)
	pass := passwords{}
	user := mod.User{}

	_ = decoder.Decode(&pass)
	log.Print(pass)

	user.Id = pass.Id
	user.GetById()

	if user.ChangePassword(pass.NewPass, editorId) {
		result, _ := json.Marshal(user)
		response(w, 200, string(result))
		return
	}
	response(w, 400, "ERROR")
}

/*
	With this function you can change the status of a user from the database
*/
func UpdateStatus(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	editorId, _ := strconv.Atoi(params["id"])

	decoder := json.NewDecoder(r.Body)
	user := mod.User{}

	_ = decoder.Decode(&user)

	if user.ChangeStatus(editorId) {
		user.GetById()
		result, _ := json.Marshal(user)
		response(w, 200, string(result))
		return
	}
	response(w, 400, "ERROR")
}

package routes

/*
	This package contains functions needed
	in the index of the application.
*/
import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	@w is the variable needed for the return of data,
	@r is just needed to return the http response
*/

/*
	This function returns all the information of a idtype
	by id
*/
func GetIdType(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	idtypeId, _ := strconv.Atoi(params["id"])

	idtype := mod.IdType{}
	idtype.Id = idtypeId
	err := idtype.GetById()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(idtype)
	response(w, 200, string(result))
}

/*
	This function returns all the idtypees
*/
func GetIdTypees(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	idtype := mod.IdType{}
	idtypees, err := idtype.GetAll()

	if err != nil {
		response(w, 400, "Error")
		return
	}
	result, _ := json.Marshal(idtypees)
	response(w, 200, string(result))

}

/*
	With this function you can create a new idtype into the database
*/
func NewIdType(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	decoder := json.NewDecoder(r.Body)
	idtype := mod.IdType{}

	_ = decoder.Decode(&idtype)
	idtype.Id = idtype.Add()
	if idtype.Id != 0 {
		result, _ := json.Marshal(idtype)
		response(w, 201, string(result))
		return
	}
	response(w, 400, "Error")
}

/*
	With this function you can delete a idtype from the database
*/
func DeleteIdType(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	params := mux.Vars(r)
	idtypeId, _ := strconv.Atoi(params["id"])

	idtype := mod.IdType{}
	idtype.Id = idtypeId
	if idtype.Delete() {
		response(w, 200, "DELETED")
		r.Body.Close()
		return
	}
	response(w, 400, "NOT DELETED")
}

/*
	With this function you can delete a idtype from the database
*/
func UpdateIdType(w http.ResponseWriter, r *http.Request) {
	if !authorize(w, r) {
		return
	}
	decoder := json.NewDecoder(r.Body)
	idtype := mod.IdType{}

	_ = decoder.Decode(&idtype)

	if idtype.Update() {
		result, _ := json.Marshal(idtype)
		response(w, 200, string(result))
		return
	}
	response(w, 400, "ERROR")
}

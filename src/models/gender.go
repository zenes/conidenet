package models

/*
	This struct is the representation of the table gender
	in the database
*/

import (
	"log"

	con "gitlab.com/zenes/conidenet/src/models/connection"
)

type genderType string

const (
	Non_binary genderType = "Non_binary"
	Binary     genderType = "Binary"
)

type Gender struct {
	Id   int        `db:"id" json:"id"`
	Name string     `db:"name" json:"name"`
	Type genderType `db:"type" json:"type"`
}

/*
	This function returns one gender by its id,
	take in mind that it uses the sql library so for that reason
	it uses the method *sqlx.DB.GET in order to return only one result
*/
func (gen *Gender) GetById() error {
	con.StartConnection()

	sql := "SELECT id, name, type FROM gender WHERE id = ?"
	err := con.Db.Get(gen, sql, gen.Id)
	if err != nil {
		log.Print("The gender cannot be found: ")
		log.Println(err)
	}
	return err
}

/*
	This function retorns every gender in the database
	this time it uses the method *sqlx.DB.Select in order to get
	more than one result
*/
func (gen *Gender) GetAll() ([]Gender, error) {
	con.StartConnection()

	res := []Gender{}
	sql := "SELECT id, name, type FROM gender"
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, genders cannot be found or accessed: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	This function adds a value into the database
	this time the method used to do this is the method NamedExec

	@var id is a variable created in order to save the last id inserted in the
	database.
*/

func (gen *Gender) Add() int {
	con.StartConnection()
	var id int64

	tx := con.Db.MustBegin()
	sql := `INSERT INTO gender (name, type) VALUES (:name, :type)`
	res, err := tx.NamedExec(sql, &gen)
	count, _ := res.RowsAffected()

	if err != nil || count == 0 {
		log.Print("Error, the gender was not created in the db: ")
		log.Println(err)
		return 0
	}
	tx.Commit()
	id, err = res.LastInsertId()
	return int(id)
}

/*
	This function deletes a row from the database
	USE IT CAREFULLY
*/

func (gen *Gender) Delete() bool {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM gender WHERE id = :id`
	res, err := tx.NamedExec(sql, &gen)

	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, the gender was not deleted from the db: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	return true
}

/*
	This function is used to update an element in the database
*/
func (gen *Gender) Update() bool {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE gender SET name = :name, type = :type WHERE id = :id`
	_, err := tx.NamedExec(sql, &gen)

	if err != nil {
		log.Print("Error, the gender was not updated: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	return true
}

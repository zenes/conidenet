package models

/*
	This struct is the representation of the table branch
	in the database
*/

import (
	"log"

	con "gitlab.com/zenes/conidenet/src/models/connection"
)

type Branch struct {
	Id         int    `db:"id" json:"id"`
	Country_id int    `db:"country_id" json:"country_id"`
	Name       string `db:"name" json:"name"`
	Domain     string `db:"domain" json:"domain"`
	Url        string `db:"url" json:"url"`

	Country Country `json:"country"`
}

/*
	This function start the connection to the database
*/

/*
	This function returns one branch by its id,
	take in mind that it uses the sql library so for that reason
	it uses the method *sqlx.DB.GET in order to return only one result
*/
func (brch *Branch) GetById() error {
	con.StartConnection()
	sql := `SELECT id, country_id, name, domain, url
	FROM branch WHERE id = ?`
	err := con.Db.Get(brch, sql, brch.Id)
	if err != nil {
		log.Print("The branch cannot be found: ")
		log.Println(err)
		return err
	}

	brch.Country.Id = brch.Country_id
	brch.Country.GetById()

	return err
}

/*
	This function retorns every branch in the database
	this time it uses the method *sqlx.DB.Select in order to get
	more than one result
*/
func (brch *Branch) GetAll() ([]Branch, error) {
	con.StartConnection()

	res := []Branch{}
	sql := "SELECT id, country_id, name, domain, url FROM branch"
	err := con.Db.Select(&res, sql)
	for i := range res {
		res[i].Country.Id = res[i].Country_id
		res[i].Country.GetById()
	}

	if err != nil {
		log.Print("Error, branches cannot be found or accessed: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	This function adds a value into the database
	this time the method used to do this is the method NamedExec

	@var id is a variable created in order to save the last id inserted in the
	database.
*/

func (brch *Branch) Add() int {
	con.StartConnection()
	var id int64

	tx := con.Db.MustBegin()
	sql := `INSERT INTO branch (country_id, name, domain, url)
	 VALUES (:country_id, :name, :domain, :url)`
	res, err := tx.NamedExec(sql, &brch)
	count, _ := res.RowsAffected()

	if err != nil || count == 0 {
		log.Print("Error, the branch was not created in the db: ")
		log.Println(err)
		return 0
	}
	tx.Commit()
	id, err = res.LastInsertId()
	return int(id)
}

/*
	This function deletes a row from the database
	USE IT CAREFULLY
*/

func (brch *Branch) Delete() bool {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM branch WHERE id = :id`
	res, err := tx.NamedExec(sql, &brch)

	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, the branch was not deleted from the db: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	return true
}

/*
	This function is used to update an element in the database
*/
func (brch *Branch) Update() bool {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE branch SET country_id = :country_id, name = :name, 
	domain = :domain, url = :url WHERE id = :id`
	_, err := tx.NamedExec(sql, &brch)

	if err != nil {
		log.Print("Error, the branch was not updated: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	return true
}

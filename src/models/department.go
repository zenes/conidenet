package models

/*
	This struct is the representation of the table department
	in the database
*/

import (
	"database/sql"
	"log"

	con "gitlab.com/zenes/conidenet/src/models/connection"
)

type Department struct {
	Id          int            `db:"id" json:"id"`
	Branch_id   int            `db:"branch_id" json:"branch_id"`
	Name        string         `db:"name" json:"name"`
	Description sql.NullString `db:"description" json:"description"`

	Branch Branch `json:"branch"`
}

/*
	This function returns one department by its id,
	take in mind that it uses the sql library so for that reason
	it uses the method *sqlx.DB.GET in order to return only one result
*/
func (dprmnt *Department) GetById() error {
	con.StartConnection()

	sql := `SELECT id, branch_id, name, description 
	FROM department WHERE id = ?`
	err := con.Db.Get(dprmnt, sql, dprmnt.Id)

	if err != nil {
		log.Print("The department cannot be found: ")
		log.Println(err)
		return err
	}

	dprmnt.Branch.Id = dprmnt.Branch_id
	dprmnt.Branch.GetById()

	return err
}

/*
	This function retorns every department in the database
	this time it uses the method *sqlx.DB.Select in order to get
	more than one result
*/
func (dprmnt *Department) GetAll() ([]Department, error) {
	con.StartConnection()

	res := []Department{}
	sql := "SELECT id, branch_id, name, description FROM department"
	err := con.Db.Select(&res, sql)

	for i := range res {
		res[i].Branch.Id = res[i].Branch_id
		res[i].Branch.GetById()
	}

	if err != nil {
		log.Print("Error, departments cannot be found or accessed: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	This function adds a value into the database
	this time the method used to do this is the method NamedExec

	@var id is a variable created in order to save the last id inserted in the
	database.
*/

func (dprmnt *Department) Add() int {
	con.StartConnection()
	var id int64

	tx := con.Db.MustBegin()
	sql := `INSERT INTO department (branch_id, name, description)
	 VALUES (:branch_id, :name, :description)`
	res, err := tx.NamedExec(sql, &dprmnt)

	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, the department was not created in the db: ")
		log.Println(err)
		return 0
	}
	tx.Commit()
	id, err = res.LastInsertId()
	return int(id)
}

/*
	This function deletes a row from the database
	USE IT CAREFULLY
*/

func (dprmnt *Department) Delete() bool {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM department WHERE id = :id`
	res, err := tx.NamedExec(sql, &dprmnt)
	count, _ := res.RowsAffected()

	if err != nil || count == 0 {
		log.Print("Error, the department was not deleted from the db: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	return true
}

/*
	This function is used to update an element in the database
*/
func (dprmnt *Department) Update() bool {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE department SET branch_id = :branch_id, name = :name, 
	description = :description WHERE id = :id`
	_, err := tx.NamedExec(sql, &dprmnt)

	if err != nil {
		log.Print("Error, the department was not updated: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	return true
}

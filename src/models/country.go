package models

/*
	This struct is the representation of the table country
	in the database
*/

import (
	"log"

	con "gitlab.com/zenes/conidenet/src/models/connection"
)

type Country struct {
	Id   int    `db:"id" json:"id"`
	Name string `db:"name" json:"name"`
}

/*
	This function returns one country by its id,
	take in mind that it uses the sql library so for that reason
	it uses the method *sqlx.DB.GET in order to return only one result
*/
func (ctry *Country) GetById() error {
	con.StartConnection()

	sql := "SELECT id, name FROM country WHERE id = ?"
	err := con.Db.Get(ctry, sql, ctry.Id)
	if err != nil {
		log.Print("The country cannot be found: ")
		log.Println(err)
	}
	return err
}

/*
	This function retorns every country in the database
	this time it uses the method *sqlx.DB.Select in order to get
	more than one result
*/
func (ctry *Country) GetAll() ([]Country, error) {
	con.StartConnection()

	res := []Country{}
	sql := "SELECT id, name FROM country"
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, countries cannot be found or accessed: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	This function adds a value into the database
	this time the method used to do this is the method NamedExec

	@var id is a variable created in order to save the last id inserted in the
	database.
*/

func (ctry *Country) Add() int {
	con.StartConnection()
	var id int64

	tx := con.Db.MustBegin()
	sql := `INSERT INTO country (name) VALUES (:name)`
	res, err := tx.NamedExec(sql, &ctry)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, the country was not created in the db: ")
		log.Println(err)
		return 0
	}
	tx.Commit()
	id, err = res.LastInsertId()
	return int(id)
}

/*
	This function deletes a row from the database
	USE IT CAREFULLY
*/

func (ctry *Country) Delete() bool {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM country WHERE id = :id`
	res, err := tx.NamedExec(sql, &ctry)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, the country was not deleted from the db: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	return true
}

/*
	This function is used to update an element in the database
*/
func (ctry *Country) Update() bool {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE country SET name = :name WHERE id = :id`
	_, err := tx.NamedExec(sql, &ctry)
	if err != nil {
		log.Print("Error, the country was not updated: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	return true
}

package models

/*
	This struct is the representation of the table user
	in the database
*/

import (
	"crypto/sha1"
	"database/sql"
	"encoding/hex"
	"fmt"
	"log"
	"math/rand"
	"strings"

	"golang.org/x/crypto/bcrypt"

	con "gitlab.com/zenes/conidenet/src/models/connection"
)

type status string

const (
	ACTIVE   status = "ACTIVE"
	INACTIVE status = "INACTIVE"
)

type User struct {
	Id            int            `db:"id" json:"id"`
	IdType_Id     int            `db:"idtype_id" json:"idtype_id"`
	Department_id int            `db:"department_id" json:"department_id"`
	Gender_id     int            `db:"gender_id" json:"gender_id"`
	Employeeid    string         `db:"employeeid" json:"employeeid"`
	Documentid    string         `db:"documentid" json:"documentid"`
	Password      string         `db:"password" json:"password"`
	Email         string         `db:"email" json:"email"`
	Name          string         `db:"name" json:"name"`
	SecondName    sql.NullString `db:"secondname" json:"secondname"`
	Surname       string         `db:"surname" json:"surname"`
	LastName      string         `db:"lastname" json:"lastname"`
	Birthdate     sql.NullString `db:"birthdate" json:"birthdate"`
	Startdate     string         `db:"startdate" json:"startdate"`
	RegDate       string         `db:"regdate" json:"regdate"`
	Status        status         `db:"status" json:"status"`

	DocumentType IdType     `json:"document_type"`
	Department   Department `json:"department"`
	Gender       Gender     `json:"gender"`
}

/*
	This functions adds a log everytime that the user is updated
	or changed
*/
func (usr *User) addLog(idEditor int) int {
	lg := EditLog{}
	lg.Editor_id = idEditor
	lg.User_id = usr.Id

	return lg.Add()

}

/*
	This function apply a hash to a string in order to create
	an unique string that can be stored as a password
*/
func (usr *User) genPassword() error {
	hash, err := bcrypt.GenerateFromPassword([]byte(usr.Password), bcrypt.MinCost)
	usr.Password = string(hash)
	return err
}

/*
	This function generates an unique employee id for every user
*/
func (usr *User) genEmpId() {
	hash := sha1.New()
	random := fmt.Sprint(rand.Intn(99999))
	hash.Write([]byte(usr.Documentid + random))
	usr.Employeeid = hex.EncodeToString(hash.Sum(nil))
}

/*
	This function follows the pattern given by the project requirements
	<firstName>.<surName>.id@<orgDomain>

	It selects all the users that have the same name/surname and
	if the result exist, then it creates a new email following the
	id secuence using the length of the array of selected users
	removing all whitespaces in the proccess, and at the end
	it transform the entire string into lower case.
*/
func (usr *User) genMailAdress() {
	users := []User{}
	var tail string = ""
	var size int

	sql := `SELECT id, name, surname FROM user 
	WHERE name LIKE ? AND surname LIKE ? AND id != ?`
	_ = con.Db.Select(&users, sql, usr.Name, usr.Surname, usr.Id)

	size = len(users)
	if size > 0 {
		tail = "." + fmt.Sprint(size)
	}

	usr.Department.Id = usr.Department_id
	usr.Department.GetById()

	/*
		I does this with fmt.Sprintf
		because is more easy to understand
		compared to a concatenation
		made by hand
	*/
	email := fmt.Sprintf("%s.%s%s@%s",
		strings.ReplaceAll(usr.Name, " ", ""),
		strings.ReplaceAll(usr.Surname, " ", ""),
		tail, usr.Department.Branch.Domain)

	usr.Email = strings.ToLower(email)

}

/*
	This function returns one user by its id,
	take in mind that it uses the sql library so for that reason
	it uses the method *sqlx.DB.GET in order to return only one result
*/
func (usr *User) GetById() error {
	con.StartConnection()

	sql := `SELECT idtype_id, department_id, gender_id,
	employeeid, documentid, password, email, name, secondname, 
	surname, lastname, birthdate, startdate, regdate, status
	FROM user WHERE id = ?`
	err := con.Db.Get(usr, sql, usr.Id)
	if err != nil {
		log.Print("The user cannot be found: ")
		log.Println(err)
		return err
	}

	usr.DocumentType.Id = usr.IdType_Id
	usr.Department.Id = usr.Department_id
	usr.Gender.Id = usr.Gender_id

	usr.DocumentType.GetById()
	usr.Department.GetById()
	usr.Gender.GetById()

	return err
}

/*
	This function retorns every user in the database
	this time it uses the method *sqlx.DB.Select in order to get
	more than one result
*/
func (usr *User) GetAll() ([]User, error) {
	con.StartConnection()

	res := []User{}
	sql := `SELECT id, idtype_id, department_id, gender_id,
	employeeid, documentid, email, name, secondname, 
	surname, lastname, birthdate, startdate, regdate, status
	FROM user`
	err := con.Db.Select(&res, sql)

	for i := range res {
		res[i].DocumentType.Id = res[i].IdType_Id
		res[i].Department.Id = res[i].Department_id
		res[i].Gender.Id = res[i].Gender_id

		res[i].DocumentType.GetById()
		res[i].Department.GetById()
		res[i].Gender.GetById()
	}

	if err != nil {
		log.Print("Error, users cannot be found or accessed: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	This function adds a value into the database
	this time the method used to do this is the method NamedExec


	@var id is a variable created in order to save the last id inserted in the
	database.
*/

func (usr *User) Add() int {
	con.StartConnection()
	var id int64
	var err error

	usr.genMailAdress()
	usr.genEmpId()

	err = usr.genPassword()
	if err != nil {
		return 0
	}

	tx := con.Db.MustBegin()
	sql := `INSERT INTO user (idtype_id, department_id, gender_id, employeeid, 
	documentid, password, email, name, secondname, surname, lastname, birthdate,
	startdate)
	 VALUES (:idtype_id, :department_id, :gender_id, :employeeid,
	:documentid, :password, :email, :name, :secondname, 
	:surname, :lastname, :birthdate, :startdate)`
	res, err := tx.NamedExec(sql, &usr)

	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, the user was not created in the db: ")
		log.Println(err)
		return 0
	}
	tx.Commit()
	id, err = res.LastInsertId()

	return int(id)
}

/*
	This function deletes a row from the database
	USE IT CAREFULLY
*/

func (usr *User) Delete() bool {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM user WHERE id = :id`
	res, err := tx.NamedExec(sql, &usr)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, the user was not deleted from the db: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	return true
}

/*
	This function is used to update an element in the database
*/
func (usr *User) Update(idEditor int) bool {
	con.StartConnection()

	usr.genMailAdress()
	usr.genEmpId()

	tx := con.Db.MustBegin()
	sql := `UPDATE user SET idtype_id = :idtype_id, 
	department_id = :department_id, gender_id = :gender_id, 
	employeeid = :employeeid, documentid = :documentid, 
	email = :email, name = :name, secondname = :secondname, 
	surname = :surname, lastname = :lastname, birthdate = :birthdate
	WHERE id = :id`
	res, err := tx.NamedExec(sql, &usr)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, the user was not updated: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	_ = usr.addLog(idEditor)
	return true
}

/*
	This function updates the password for the user

	This function gets the new password as a parameter and then
	it compares the dabatase hash with the new password, if the comparison returns true
	then it doesn't update the password.
*/

func (usr *User) ChangePassword(newPass string, idEditor int) bool {
	con.StartConnection()
	usr.GetById()

	err := bcrypt.CompareHashAndPassword([]byte(usr.Password), []byte(newPass))
	if err == nil {
		log.Print("The new password is the same as the old one")
		return false
	}

	usr.Password = newPass
	usr.genPassword()
	tx := con.Db.MustBegin()
	sql := "UPDATE user SET password = :password WHERE id = :id"
	res, err := tx.NamedExec(sql, &usr)

	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, the password was not updated: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	_ = usr.addLog(idEditor)

	return true
}

/*
	This function enables or disables one user by id
*/
func (usr *User) ChangeStatus(idEditor int) bool {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := "UPDATE user SET status = :status WHERE id = :id"
	res, err := tx.NamedExec(sql, &usr)
	count, _ := res.RowsAffected()

	if err != nil || count == 0 {
		log.Print("Error, the user status was not updated: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	_ = usr.addLog(idEditor)

	return true
}

/*
	This function is used to login
*/
func (usr *User) Login(password string) bool {
	con.StartConnection()

	sql := `SELECT id, email, password, name, 
	employeeid, documentid
	FROM user WHERE email = ?`
	err := con.Db.Get(usr, sql, usr.Email)
	if err != nil {
		log.Print("The user cannot be found: ")
		log.Println(err)
		return false
	}

	err = bcrypt.CompareHashAndPassword([]byte(usr.Password), []byte(password))
	if err != nil {
		log.Print("Invalid password")
		return false
	}
	return true

}

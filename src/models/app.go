package models

/*
	This struct is the representation of the table app
	in the database

	I this table you cannot update/delete or add information
	it just have the app key for the front-end
*/

import (
	"log"

	con "gitlab.com/zenes/conidenet/src/models/connection"
)

/*
	@var Name is a unique identifier for an app and it has nothing
	to do with the @Auth value.
	@Auth Is the api token used to validate the app
*/
type App struct {
	Id   int    `db:"id" `
	Name string `db:"name"`
	Auth string `db:"auth" `
}

/*
	This function returns the app element based in the app identifier
	@Name
*/
func (app *App) GetByName() error {
	con.StartConnection()

	sql := `SELECT id, name, auth FROM app WHERE name = ?`
	err := con.Db.Get(app, sql, app.Name)
	if err != nil {
		log.Print("The app cannot be found: ")
		log.Println(err)
	}
	return err
}

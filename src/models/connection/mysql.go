package connection

/*
	Connection package to the database
*/
import (
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var Db *sqlx.DB

const (
	dbName     = "conidenet"
	dbUser     = "conidenet"
	dbPassword = ""
)

func StartConnection() {
	var err error

	Db, err = sqlx.Open("mysql", dbUser+":"+dbPassword+"@tcp(127.0.0.1:3306)/"+dbName+"?parseTime=false")

	if err != nil {
		log.Println("Failed to connect to database: ")
		log.Panic(err)
	} else {
		err = Db.Ping()
		if err != nil {
			log.Println("Failed to ping database: ")
			log.Panic(err)
		} else {
			Db.SetConnMaxLifetime(60)
			log.Println("Connection stablished.")
		}
	}
}

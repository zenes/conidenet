package models

/*
	This struct is the representation of the table editlog
	in the database
*/

import (
	"log"

	con "gitlab.com/zenes/conidenet/src/models/connection"
)

type EditLog struct {
	Id        int    `db:"id" json:"id"`
	Editor_id int    `db:"editor_id" json:"editor_id"`
	User_id   int    `db:"user_id" json:"user_id"`
	Date      string `db:"date" json:"date"`

	Editor User `json:"editor"`
	Usr    User `json:"user"`
}

/*
	This function returns one editlog by its id,
	take in mind that it uses the sql library so for that reason
	it uses the method *sqlx.DB.GET in order to return only one result
*/
func (editlg *EditLog) GetById() error {
	con.StartConnection()

	sql := "SELECT id, editor_id, user_id, date FROM editlog WHERE id = ?"
	err := con.Db.Get(editlg, sql, editlg.Id)
	if err != nil {
		log.Print("The log cannot be found: ")
		log.Println(err)
		return err
	}
	editlg.Editor.Id = editlg.Editor_id
	editlg.Editor.GetById()
	editlg.Editor.Employeeid = ""
	editlg.Editor.Password = ""

	editlg.Usr.Id = editlg.User_id
	editlg.Usr.GetById()
	editlg.Usr.Employeeid = ""
	editlg.Usr.Password = ""

	return err
}

/*
	This function retorns every editlog in the database
	this time it uses the method *sqlx.DB.Select in order to get
	more than one result
*/
func (editlg *EditLog) GetAll() ([]EditLog, error) {
	con.StartConnection()

	res := []EditLog{}
	sql := "SELECT id, editor_id, user_id, date FROM editlog"
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, logs cannot be found or accessed: ")
		log.Println(err)
		return nil, err
	}
	for i := range res {
		res[i].Editor.Id = res[i].Editor_id
		res[i].Editor.GetById()
		res[i].Editor.Employeeid = ""
		res[i].Editor.Password = ""

		res[i].Usr.Id = res[i].User_id
		res[i].Usr.GetById()
		res[i].Usr.Employeeid = ""
		res[i].Usr.Password = ""
	}
	return res, err
}

/*
	This function adds a value into the database
	this time the method used to do this is the method NamedExec

	@var id is a variable created in order to save the last id inserted in the
	database.
*/

func (editlg *EditLog) Add() int {
	con.StartConnection()
	var id int64

	tx := con.Db.MustBegin()
	sql := `INSERT INTO editlog (editor_id, user_id) VALUES (:editor_id, :user_id)`
	res, err := tx.NamedExec(sql, &editlg)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, the log was not created in the db: ")
		log.Println(err)
		return 0
	}
	tx.Commit()
	id, err = res.LastInsertId()
	return int(id)
}

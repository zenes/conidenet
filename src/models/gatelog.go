package models

/*
	This struct is the representation of the table gatelog
	in the database
*/

import (
	"log"

	con "gitlab.com/zenes/conidenet/src/models/connection"
)

type actionType string

const (
	IN  actionType = "IN"
	OUT actionType = "OUT"
)

type GateLog struct {
	Id      int        `db:"id" json:"id"`
	User_id int        `db:"user_id" json:"user_id"`
	Date    string     `db:"date" json:"date"`
	Type    actionType `db:"type" json:"type"`

	User User `json:"user"`
}

/*
	This function returns one gatelog by its id,
	take in mind that it uses the sql library so for that reason
	it uses the method *sqlx.DB.GET in order to return only one result
*/
func (io *GateLog) GetById() error {
	con.StartConnection()

	sql := "SELECT id, user_id, date, type FROM gatelog WHERE id = ?"
	err := con.Db.Get(io, sql, io.Id)
	if err != nil {
		log.Print("The gatelog cannot be found: ")
		log.Println(err)
		return err
	}

	io.User.Id = io.User_id
	io.User.GetById()
	io.User.Employeeid = ""
	io.User.Password = ""
	return err
}

/*
	This function retorns every gatelog in the database
	this time it uses the method *sqlx.DB.Select in order to get
	more than one result
*/
func (io *GateLog) GetAll() ([]GateLog, error) {
	con.StartConnection()

	res := []GateLog{}
	sql := "SELECT id, user_id, date, type FROM gatelog"
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, gatelogs cannot be found or accessed: ")
		log.Println(err)
		return nil, err
	}

	for i := range res {
		res[i].User.Id = res[i].User_id
		res[i].User.GetById()
		res[i].User.Employeeid = ""
		res[i].User.Password = ""
	}
	return res, err
}

/*
	This function adds a value into the database
	this time the method used to do this is the method NamedExec

	@var id is a variable created in order to save the last id inserted in the
	database.
*/

func (io *GateLog) Add() int {
	con.StartConnection()
	var id int64

	tx := con.Db.MustBegin()
	sql := `INSERT INTO gatelog (user_id, type) VALUES (:user_id, :type)`
	res, err := tx.NamedExec(sql, &io)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, the gatelog was not created in the db: ")
		log.Println(err)
		return 0
	}
	tx.Commit()
	id, err = res.LastInsertId()
	return int(id)
}

package models

/*
	This struct is the representation of the table idtype
	in the database
*/

import (
	"log"

	con "gitlab.com/zenes/conidenet/src/models/connection"
)

type IdType struct {
	Id   int    `db:"id" json:"id"`
	Name string `db:"name" json:"name"`
}

/*
	This function returns one idtype by its id,
	take in mind that it uses the sql library so for that reason
	it uses the method *sqlx.DB.GET in order to return only one result
*/
func (idType *IdType) GetById() error {
	con.StartConnection()

	sql := "SELECT id, name FROM idtype WHERE id = ?"
	err := con.Db.Get(idType, sql, idType.Id)
	if err != nil {
		log.Print("The idtype cannot be found: ")
		log.Println(err)
	}
	return err
}

/*
	This function retorns every idtype in the database
	this time it uses the method *sqlx.DB.Select in order to get
	more than one result
*/
func (idType *IdType) GetAll() ([]IdType, error) {
	con.StartConnection()

	res := []IdType{}
	sql := "SELECT id, name FROM idtype"
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, idtypes cannot be found or accessed: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	This function adds a value into the database
	this time the method used to do this is the method NamedExec

	@var id is a variable created in order to save the last id inserted in the
	database.
*/

func (idType *IdType) Add() int {
	con.StartConnection()
	var id int64

	tx := con.Db.MustBegin()
	sql := `INSERT INTO idtype (name) VALUES (:name)`
	res, err := tx.NamedExec(sql, &idType)
	if err != nil {
		log.Print("Error, the idtype was not created in the db: ")
		log.Println(err)
		return 0
	}
	tx.Commit()
	id, err = res.LastInsertId()
	return int(id)
}

/*
	This function deletes a row from the database
	USE IT CAREFULLY
*/

func (idType *IdType) Delete() bool {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM idtype WHERE id = :id`
	res, err := tx.NamedExec(sql, &idType)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, the idtype was not deleted from the db: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	return true
}

/*
	This function is used to update an element in the database
*/
func (idType *IdType) Update() bool {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE idtype SET name = :name WHERE id = :id`
	res, err := tx.NamedExec(sql, &idType)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, the idtype was not updated: ")
		log.Println(err)
		return false
	}
	tx.Commit()
	return true
}

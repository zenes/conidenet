package tests

import (
	"testing"

	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	Test for the functglogn GetAll of the gatelog model
*/
func TestGetAllGateLog(t *testing.T) {
	gatelog := mod.GateLog{}
	res, err := gatelog.GetAll()
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}
	t.Logf("Everything works, test done: %v", res)
}

/*
	Test for the functglogn GetById of the branch model

	This will be throw an error if there is no informatglogn
	in the table gatelog of the database
*/
func TestGetByIdGateLog(t *testing.T) {
	gatelog := mod.GateLog{}
	gatelog.Id = 1

	err := gatelog.GetById()
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}
	t.Logf("Everything works, test done: %v", gatelog)
}

/*
	Test for the functglogn Add of the gatelog model

	This functglogns creates a new gatelog and deletes it
	at the end in order to not crowd the database with
	useless informatglogn.

*/
func TestAddGateLog(t *testing.T) {
	gatelog := mod.GateLog{}
	gatelog.User_id = 13
	gatelog.Type = mod.IN
	// gatelog.Type = mod.OUT

	gatelog.Id = gatelog.Add()
	if gatelog.Id == 0 {
		t.Error("Log not created")
		t.Fail()
		return
	}
	gatelog.GetById()
	t.Logf("Everything works, test done: %v", gatelog)
}

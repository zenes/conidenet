package tests

import (
	"testing"

	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	Test for the function GetAll of the idtype model
*/
func TestGetAllIdType(t *testing.T) {
	idtype := mod.IdType{}
	res, err := idtype.GetAll()
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}
	t.Logf("Everything works, test done: %v", res)
}

/*
	Test for the function GetById of the branch model

	This will be throw an error if there is no information
	in the table idtype of the database
*/
func TestGetByIdIdType(t *testing.T) {
	idtype := mod.IdType{}
	idtype.Id = 1

	err := idtype.GetById()
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}
	t.Logf("Everything works, test done: %v", idtype)
}

/*
	Test for the function Add of the idtype model

	This functions creates a new idtype and deletes it
	at the end in order to not crowd the database with
	useless information.

*/
func TestAddIdType(t *testing.T) {
	idtype := mod.IdType{}
	idtype.Name = "TEST"

	idtype.Id = idtype.Add()
	if idtype.Id == 0 {
		t.Error("IdType not added")
		t.Fail()
		return
	}
	idtype.GetById()
	t.Logf("Everything works, test done: %v", idtype)
	_ = idtype.Delete()
}

/*
	Test for the function Delete of the idtype model

	This function Creates a idtype and at the end it
	deletes it in order to not delete some important
	information that the db can have.
*/
func TestDeleteIdType(t *testing.T) {
	idtype := mod.IdType{}
	idtype.Name = "Test"

	idtype.Id = idtype.Add()

	if idtype.Delete() {
		t.Logf("Everything works, test done")
		return
	}
	t.Error("IdType not deleted")
	t.Fail()
}

/*
	Test for the function Update of the idtype model

	This function creates a new idtype to update and then
	it deteles it in order to mantain the database unchanged.
*/
func TestUpdateIdType(t *testing.T) {
	idtype := mod.IdType{}
	var err error

	idtype.Name = "Test"
	idtype.Id = idtype.Add()
	idtype.Name = "Test2"

	if idtype.Update() {
		_ = idtype.GetById()
		t.Logf("Everything works, test done: %v", idtype)
		_ = idtype.Delete()
		return
	}
	t.Error(err)
	t.Fail()
}

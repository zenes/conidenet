package tests

import (
	"testing"

	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	test for the function GetAll of the gender model
*/
func TestGetAllGen(t *testing.T) {
	gender := mod.Gender{}
	res, err := gender.GetAll()
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}
	t.Logf("Everything works, test done: %v", res)
}

/*
	Test for the function GetById of the branch model

	This will be throw an error if there is no information
	in the table gender of the database
*/
func TestGetByIdGen(t *testing.T) {
	gender := mod.Gender{}
	gender.Id = 1

	err := gender.GetById()
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}
	t.Logf("Everything works, test done: %v", gender)
}

/*
	Test for the function Add of the gender model

	This functions creates a new gender and deletes it
	at the end in order to not crowd the database with
	useless information.

*/
func TestAddGen(t *testing.T) {
	gender := mod.Gender{}
	gender.Name = "Test"
	gender.Type = mod.Binary

	gender.Id = gender.Add()

	if gender.Id == 0 {
		t.Error("Gender not created")
		t.Fail()
		return
	}
	gender.GetById()
	t.Logf("Everything works, test done: %v", gender)
	_ = gender.Delete()
}

/*
	Test for the function Delete of the gender model

	This function Creates a gender and at the end it
	deletes it in order to not delete some important
	information that the db can have.
*/
func TestDeleteGen(t *testing.T) {
	gender := mod.Gender{}
	gender.Name = "Test"
	gender.Type = mod.Non_binary

	gender.Id = gender.Add()

	if gender.Delete() {
		t.Logf("Everything works, test done")
		return
	}
	t.Error("Gender not deleted")
	t.Fail()
}

/*
	Test for the function Update of the gender model

	This function creates a new gender to update and then
	it deteles it in order to mantain the database unchanged.
*/
func TestUpdateGen(t *testing.T) {
	gender := mod.Gender{}
	var err error

	gender.Name = "Test"
	gender.Type = mod.Binary

	gender.Id = gender.Add()
	gender.Name = "TEST2"
	gender.Type = mod.Non_binary

	if gender.Update() {
		_ = gender.GetById()
		t.Logf("Everything works, test done: %v", gender)
		_ = gender.Delete()
		return
	}
	t.Error(err)
	t.Fail()
}

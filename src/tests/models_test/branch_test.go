package tests

import (
	"testing"

	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	Test for the function GetAll of the branch model
*/
func TestGetAllBrch(t *testing.T) {
	branch := mod.Branch{}
	res, err := branch.GetAll()
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}
	t.Logf("Everything works, test done: %v", res)
}

/*
	Test for the function GetById of the branch model

	This will be throw an error if there is no information
	in the table branch of the database
*/
func TestGetByIdBrch(t *testing.T) {
	branch := mod.Branch{}
	branch.Id = 2

	err := branch.GetById()
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}
	t.Logf("Everything works, test done: %v", branch)
}

/*
	Test for the function Add of the branch model

	This functions creates a new branch and deletes it
	at the end in order to not crowd the database with
	useless information.

*/
func TestAddBrch(t *testing.T) {
	branch := mod.Branch{}
	branch.Country_id = 2
	branch.Name = "Test"
	branch.Domain = "Test"
	branch.Url = "Test"

	branch.Id = branch.Add()
	if branch.Id == 0 {
		t.Error("Branch not created")
		t.Fail()
		return
	}
	branch.GetById()
	t.Logf("Everything works, test done: %v", branch)
	_ = branch.Delete()
}

/*
	Test for the function Delete of the branch model

	This function Creates a branch and at the end it
	deletes it in order to not delete some important
	information that the db can have.
*/
func TestDeleteBrch(t *testing.T) {
	branch := mod.Branch{}
	branch.Country_id = 2
	branch.Name = "Test"
	branch.Domain = "Test"
	branch.Url = "Test"

	id := branch.Add()
	branch.Id = id

	if branch.Delete() {
		t.Logf("Everything works, test done")
		return
	}
	t.Error("Error, the Branch was not deleted")
	t.Fail()
}

/*
	Test for the function Update of the branch model

	This function creates a new branch to update and then
	it deteles it in order to mantain the database unchanged.
*/
func TestUpdateBrch(t *testing.T) {
	branch := mod.Branch{}
	var err error

	branch.Country_id = 2
	branch.Name = "Test"
	branch.Domain = "Test"
	branch.Url = "Test"

	id := branch.Add()
	branch.Id = id
	branch.Country_id = 1
	branch.Name = "Test2"
	branch.Domain = "Test2"
	branch.Url = "Test2"

	if branch.Update() {
		branch.GetById()
		t.Logf("Everything works, test done: %v", branch)
		_ = branch.Delete()
		return
	}

	t.Error(err)
	t.Fail()
}

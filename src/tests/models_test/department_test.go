package tests

import (
	"testing"

	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	Test for the function GetAll of the department model
*/
func TestGetAllDprmnt(t *testing.T) {
	department := mod.Department{}
	res, err := department.GetAll()
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}
	t.Logf("Everything works, test done: %v", res)
}

/*
	Test for the function GetById of the department model

	This will be throw an error if there is no information
	in the table department of the database
*/
func TestGetByIdDprmnt(t *testing.T) {
	department := mod.Department{}
	department.Id = 2

	err := department.GetById()
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}
	t.Logf("Everything works, test done: %v", department)
}

/*
	Test for the function Add of the department model

	This functions creates a new department and deletes it
	at the end in order to not crowd the database with
	useless information.

*/
func TestAddDprmnt(t *testing.T) {
	department := mod.Department{}
	department.Branch_id = 2
	department.Name = "Test"
	department.Description.String = "Test"
	department.Description.Valid = true

	department.Id = department.Add()
	if department.Id == 0 {
		t.Error("The department was not created")
		t.Fail()
		return
	}
	res := department.GetById()
	t.Logf("Everything works, test done: %v", res)
	_ = department.Delete()
}

/*
	Test for the function Delete of the department model

	This function Creates a department and at the end it
	deletes it in order to not delete some important
	information that the db can have.
*/
func TestDeleteDprmnt(t *testing.T) {
	department := mod.Department{}
	department.Branch_id = 2
	department.Name = "Test"
	department.Description.String = "Test"

	department.Id = department.Add()

	if department.Delete() {
		t.Logf("Everything works, test done")
		return
	}
	t.Error("Department not deleted")
	t.Fail()
}

/*
	Test for the function Update of the department model

	This function creates a new department to update and then
	it deteles it in order to mantain the database unchanged.
*/
func TestUpdateDprmnt(t *testing.T) {
	department := mod.Department{}
	var err error

	department.Branch_id = 2
	department.Name = "Test"
	department.Description.String = "Test"

	department.Id = department.Add()

	department.Branch_id = 1
	department.Name = "Test2"
	department.Description.String = "Test2"

	if department.Update() {
		department.GetById()
		t.Logf("Everything works, test done: %v", department)
		_ = department.Delete()
		return
	}
	t.Error(err)
	t.Fail()
}

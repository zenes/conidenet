package tests

import (
	"testing"

	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	Test for the function GetAll of the country model
*/
func TestGetAllCtry(t *testing.T) {
	country := mod.Country{}
	res, err := country.GetAll()
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}
	t.Logf("Everything works, test done: %v", res)
}

/*
	Test for the function GetById of the branch model

	This will be throw an error if there is no information
	in the table country of the database
*/
func TestGetByIdCtry(t *testing.T) {
	country := mod.Country{}
	country.Id = 1

	err := country.GetById()
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}
	t.Logf("Everything works, test done: %v", country)
}

/*
	Test for the function Add of the country model

	This functions creates a new country and deletes it
	at the end in order to not crowd the database with
	useless information.

*/
func TestAddCtry(t *testing.T) {
	country := mod.Country{}
	country.Name = "Nigeria"

	country.Id = country.Add()
	if country.Id != 0 {
		_ = country.GetById()
		t.Logf("Everything works, test done: %v", country)
		_ = country.Delete()
		return
	}
	t.Error("Error the country was not created")
	t.Fail()
}

/*
	Test for the function Delete of the country model

	This function Creates a country and at the end it
	deletes it in order to not delete some important
	information that the db can have.
*/
func TestDeleteCtry(t *testing.T) {
	country := mod.Country{}
	country.Name = "Siria"

	country.Id = country.Add()

	if country.Delete() {
		t.Logf("Everything works, test done")
		return
	}
	t.Error("The country was not deleted")
	t.Fail()
}

/*
	Test for the function Update of the country model

	This function creates a new country to update and then
	it deteles it in order to mantain the database unchanged.
*/
func TestUpdateCtry(t *testing.T) {
	country := mod.Country{}
	var err error

	country.Name = "Surinam"
	country.Id = country.Add()
	country.Name = "China"

	if country.Update() {
		_ = country.GetById()
		t.Logf("Everything works, test done: %v", country)
		_ = country.Delete()
		return
	}
	t.Error(err)
	t.Fail()
}

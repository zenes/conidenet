package tests

import (
	"testing"

	mod "gitlab.com/zenes/conidenet/src/models"
)

/*
	Test for the function GetAll of the user model
*/
func TestGetAllUsr(t *testing.T) {
	usr := mod.User{}
	res, err := usr.GetAll()
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	t.Logf("Everything works, test done: %v", res)
}

/*
	Test for the function GetById of the user model

	This will be throw an error if there is no information
	in the table user of the database
*/
func TestGetByIdUsr(t *testing.T) {
	usr := mod.User{}
	usr.Id = 13

	err := usr.GetById()
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}
	t.Logf("Everything works, test done: %v", usr)
}

/*
	Test for the function Add of the usr model

	This functions creates a new usr and deletes it
	at the end in order to not crowd the database with
	useless information.

*/
func TestAddUsr(t *testing.T) {
	usr := mod.User{}
	usr.IdType_Id = 1
	usr.Department_id = 1
	usr.Gender_id = 2
	usr.Documentid = "2222233333"
	usr.Password = "88Iai0JsbB+FnXnBHfXrN3ACMJv5eGn3H5Hov3SrXq4="
	usr.Name = "Juanito"

	usr.SecondName.String = "AASDASD"
	usr.SecondName.Valid = true

	usr.Surname = "De las casas"
	usr.LastName = "Buitron"

	usr.Birthdate.String = "1990-10-10"
	usr.Birthdate.Valid = true

	usr.Startdate = "2020-11-25"

	usr.Id = usr.Add()
	if usr.Id == 0 {
		t.Error("User not added")
		t.Fail()
		return
	}
	_ = usr.GetById()
	t.Logf("Everything works, test done: %v", usr)
	// _ = usr.Delete()
}

/*
	Test for the function Delete of the usr model

	This function Creates a usr and at the end it
	deletes it in order to not delete some important
	information that the db can have.
*/
func TestDeleteUsr(t *testing.T) {
	usr := mod.User{}
	usr.IdType_Id = 1
	usr.Department_id = 1
	usr.Gender_id = 2
	usr.Documentid = "44412234"
	usr.Password = "clave123"
	usr.Name = "Juanito"

	usr.SecondName.String = "AASDASD"
	usr.SecondName.Valid = true

	usr.Surname = "De las casas"
	usr.LastName = "Buitron"

	usr.Birthdate.String = "1990-10-10"
	usr.Birthdate.Valid = true

	usr.Startdate = "2020-11-25"

	usr.Id = usr.Add()

	if usr.Delete() {
		t.Logf("Everything works, test done")
		return
	}
	t.Error("User not deleted")
	t.Fail()
}

/*
	Test for the function Update of the usr model

	This function creates a new usr to update and then
	it deteles it in order to mantain the database unchanged.
*/
func TestUpdateUsr(t *testing.T) {
	usr := mod.User{}
	var err error

	usr.IdType_Id = 1
	usr.Department_id = 1
	usr.Gender_id = 2
	usr.Documentid = "1787238"
	usr.Password = "daLal69BV85nTEVgE9WMP5dqb0kGI0OSZoRHKU7Y0xE="
	usr.Name = "Andres"

	usr.SecondName.String = "AASDASD"
	usr.SecondName.Valid = true

	usr.Surname = "De las casas"
	usr.LastName = "Buitron"

	usr.Birthdate.String = "1990-10-10"
	usr.Birthdate.Valid = true

	usr.Startdate = "2020-11-25"
	usr.Id = usr.Add()

	usr.IdType_Id = 1
	usr.Department_id = 1
	usr.Gender_id = 2
	usr.Documentid = "33333333"
	usr.Name = "Juanito"

	usr.SecondName.String = "AASDASD"
	usr.SecondName.Valid = true

	usr.Surname = "De las casas"
	usr.LastName = "Buitron"

	usr.Birthdate.String = "1990-10-10"
	usr.Birthdate.Valid = true

	if usr.Update(13) {
		_ = usr.GetById()
		t.Logf("Everything works, test done: %v", usr)
		_ = usr.Delete()
		return
	}
	t.Error(err)
	t.Fail()
}

/*
	This function test if the password can be updated
*/
func TestChangePassUsr(t *testing.T) {
	usr := mod.User{}
	usr.Id = 13
	if !usr.ChangePassword("123444", 13) {
		t.Error("The password was not updated")
		t.Fail()
		return
	}
	_ = usr.GetById()
	t.Logf("Everything works, test done: %v", usr)

}
func TestChangeStatusUsr(t *testing.T) {
	usr := mod.User{}
	usr.Id = 13
	usr.Status = mod.ACTIVE
	// usr.Status = mod.INACTIVE

	if usr.ChangeStatus(13) {
		t.Error("User status not changed")
		t.Fail()
		return
	}
	_ = usr.GetById()
	t.Logf("Everything works, test done: %v", usr)
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { CookieService } from 'ngx-cookie-service';
import { name, version } from '../../../../package.json';



export const environment={
  name,
  version
}

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})

export class SidebarComponent implements OnInit {

  constructor(
    private router:Router,
    private cookieService: CookieService,
  ) { }

  ngOnInit(): void {
    this.checkLogin()
  }

  checkLogin(){
    if (!this.cookieService.get("token")) {
        this.router.navigate(['login']);
    }
  }

  userName:string = this.cookieService.get("name")
  appName:string = environment.name;

}

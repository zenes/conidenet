import { Component, ViewChild, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { DepartmentService } from '../../../core/services/department.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { DepartmentI } from '../../../core/interfaces/department.interface'

declare var $;

@Component({
  selector: 'app-show-departments',
  templateUrl: './show-departments.component.html',
  styleUrls: [
    './show-departments.component.css',
   '../../../../../node_modules/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css'
  ]
})
export class ShowDepartmentsComponent implements AfterViewInit, OnInit {

  constructor(
    private service:DepartmentService,
    private router:Router,
    private cookieService: CookieService
  ) { }

  departments:DepartmentI[];
  @ViewChild('dataShow') table: ElementRef;
  dataTable: any;
  
  initTable(){
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.dataTable({
      "responsive": true,
      "dom": "<'row'<'col-lg-10 col-md-10 col-xs-12'f><'col-lg-2 col-md-2 col-xs-12'l>>" +
           "<'row'<'col-sm-12'tr>>" +
           "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
           "language": {
            "search": "Buscar:",
          }
    });
  }

  ngOnInit(): void{

      if (!this.cookieService.get("token")) {
          this.router.navigate(['login']);
      }
      this.service.getAllDepartments().subscribe( response => {

        this.departments = response;

      }, (error)=>{
        console.log("ERROR");
      });
  }
  ngAfterViewInit(): void{
    setTimeout(() => {
      this.initTable();
    }, 500);
  }

}

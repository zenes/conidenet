import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIdTypeComponent } from './add-id-type.component';

describe('AddIdTypeComponent', () => {
  let component: AddIdTypeComponent;
  let fixture: ComponentFixture<AddIdTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddIdTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIdTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

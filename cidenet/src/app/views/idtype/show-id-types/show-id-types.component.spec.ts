import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowIdTypesComponent } from './show-id-types.component';

describe('ShowIdTypesComponent', () => {
  let component: ShowIdTypesComponent;
  let fixture: ComponentFixture<ShowIdTypesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowIdTypesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowIdTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

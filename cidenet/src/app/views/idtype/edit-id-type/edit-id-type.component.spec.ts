import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditIdTypeComponent } from './edit-id-type.component';

describe('EditIdTypeComponent', () => {
  let component: EditIdTypeComponent;
  let fixture: ComponentFixture<EditIdTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditIdTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditIdTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

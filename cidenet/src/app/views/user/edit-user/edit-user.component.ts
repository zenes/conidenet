import { Component, ViewChild, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { IdTypeService } from '../../../core/services/idtype.service';
import { GenderService } from '../../../core/services/gender.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { IdTypeI } from '../../../core/interfaces/idtype.interface'
import { GenderI } from '../../../core/interfaces/gender.interface'

declare var $;

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  constructor(
    private idtypeSer:IdTypeService,
    private genderSer:GenderService,
    private router:Router,
    private cookieService: CookieService
  ) { }

  document_types:IdTypeI[];
  genders:GenderI[];

  ngOnInit(): void{
      if (!this.cookieService.get("token")) {
          this.router.navigate(['login']);
      }
      this.idtypeSer.getAllIdTypes().subscribe( response => {
        this.document_types = response;

      }, (error)=>{
        console.log("ERROR");
      });

      this.genderSer.getAllGenders().subscribe( response => {
        this.genders = response;

      }, (error)=>{
        console.log("ERROR");
      });
  }

}
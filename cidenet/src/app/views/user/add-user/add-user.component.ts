import { Component, ViewChild, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { IdTypeService } from '../../../core/services/idtype.service';
import { GenderService } from '../../../core/services/gender.service';
import { DepartmentService } from '../../../core/services/department.service';
import { UserService } from '../../../core/services/user.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { NotificationService } from './../../../notification.service'
import { FormGroup, FormControl, Validators} from "@angular/forms"

import { IdTypeI } from '../../../core/interfaces/idtype.interface'
import { UserI } from '../../../core/interfaces/user.interface'
import { GenderI } from '../../../core/interfaces/gender.interface'
import { DepartmentI } from '../../../core/interfaces/department.interface'

declare var $;

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(
    private idtypeSer:IdTypeService,
    private genderSer:GenderService,
    private departmentSer:DepartmentService,
    private router:Router,
    private cookieService: CookieService,
    private userSer:UserService,
    private notification:NotificationService,
  ) { }

  document_types:IdTypeI[];
  genders:GenderI[];
  departments:DepartmentI[];
  user:UserI;

  ngOnInit(): void{
  }
  ngAfterViewInit(): void{

      if (!this.cookieService.get("token")) {
          this.router.navigate(['login']);
      }
      this.idtypeSer.getAllIdTypes().subscribe( response => {
        this.document_types = response;

      }, (error)=>{
        console.log("ERROR");
      });

      this.genderSer.getAllGenders().subscribe( response => {
        this.genders = response;

      }, (error)=>{
        console.log("ERROR");
      });

      this.departmentSer.getAllDepartments().subscribe( response => {
        this.departments = response;

      }, (error)=>{
        console.log("ERROR");
      });
  }

  regForm = new FormGroup({
    name: new FormControl('', Validators.required),
    secondname: new FormControl('', Validators.required),
    surname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    documentid: new FormControl('', Validators.required),
    idtype_id: new FormControl('', Validators.required),
    department_id: new FormControl('', Validators.required),
    birthdate: new FormControl('', Validators.required),
    gender_id: new FormControl('', Validators.required),
    startdate: new FormControl('', Validators.required),
  });

  onCreate(form:UserI){
    form.idtype_id = parseInt(form.idtype_id);
    form.department_id = parseInt(form.department_id);
    form.gender_id = parseInt(form.gender_id);
    this.userSer.addUser(form).subscribe(response => {
      let data:UserI = response;
      if (data.id){
        this.notification.showSuccess("Usuario creado con exito", "Exito");
        return;
      }

    }, (error)=>{
        this.notification.showError("El ususrio no se pudo crear", "error");
    });

  }

}
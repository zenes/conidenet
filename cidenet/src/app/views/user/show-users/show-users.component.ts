import { Component, ViewChild, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { UserService } from '../../../core/services/user.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

import { UserI } from '../../../core/interfaces/user.interface'

declare var $;

@Component({
  selector: 'app-show-users',
  templateUrl: './show-users.component.html',
  styleUrls: [
    './show-users.component.css',
   '../../../../../node_modules/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css'
  ]
})
export class ShowUsersComponent implements AfterViewInit, OnInit {

  constructor(
    private service:UserService,
    private cookieService: CookieService,
    private router:Router,
  ) { }

  users:UserI[];
  @ViewChild('dataShow') table: ElementRef;
  dataTable: any;
  
  initTable(){
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.dataTable({
      "responsive": true,
      "dom": "<'row'<'col-lg-10 col-md-10 col-xs-12'f><'col-lg-2 col-md-2 col-xs-12'l>>" +
           "<'row'<'col-sm-12'tr>>" +
           "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
           "language": {
            "search": "Buscar:",
          }
    });
  }

  ngOnInit(): void{
      if (!this.cookieService.get("token")) {
          this.router.navigate(['login']);
      }

      this.service.getAllUsers().subscribe( response => {

        this.users = response;

      }, (error)=>{
        console.log("ERROR");
      });
  }
  ngAfterViewInit(): void{
    setTimeout(() => {
      this.initTable();
    }, 500);
  }

  editUser(id){
    this.router.navigate(['editUser', id]);

  }

}

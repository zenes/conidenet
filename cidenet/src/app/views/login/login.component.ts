import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators} from "@angular/forms"
import { LoginService } from "../../core/services/login.service"
import { LoginI } from "../../core/interfaces/login.interface"
import { CookieService } from 'ngx-cookie-service';

import { Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl('', 
      Validators.required
    ),
    password: new FormControl('', 
      Validators.required
    ),
  });

  constructor(private service:LoginService, 
    private router:Router,
    private cookieService: CookieService
    ) { }

    errorStatus:boolean = false;
    errorMsj:string = "";

  ngOnInit(): void {
    this.checkLogin()
  }

  checkLogin(){
    if (this.cookieService.get("token")) {
        this.router.navigate(['dashboard']);
    }
  }
  
  onLogin(form:LoginI){
    this.service.loginByEmail(form).subscribe(response =>{
      let data:LoginI = response;
      if (data.id) {

        this.cookieService.set("token", data.token, 1);
        this.cookieService.set("email", data.email, 1);
        this.cookieService.set("name", data.name, 1);
        this.cookieService.set("document_id", data.document_id, 1);

        this.router.navigate(['dashboard']);

      }
    }, (error)=>{
      this.errorStatus =true;
      this.errorMsj = "Error! usuario o contraseña invalidos";
    });
  }

}

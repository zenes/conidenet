import { Component, ViewChild, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { EditLogService } from '../../../core/services/editlog.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { EditLogI } from '../../../core/interfaces/editlog.interface'

declare var $;

@Component({
  selector: 'app-show-edit-log',
  templateUrl: './show-edit-log.component.html',
  styleUrls: [
    './show-edit-log.component.css',
   '../../../../../node_modules/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css'
  ]
})
export class ShowEditLogComponent implements AfterViewInit, OnInit {

  constructor(
    private service:EditLogService,
    private router:Router,
    private cookieService: CookieService
  ) { }

  editlogs:EditLogI[];
  @ViewChild('dataShow') table: ElementRef;
  dataTable: any;
  
  initTable(){
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.dataTable({
      "responsive": true,
      "dom": "<'row'<'col-lg-10 col-md-10 col-xs-12'f><'col-lg-2 col-md-2 col-xs-12'l>>" +
           "<'row'<'col-sm-12'tr>>" +
           "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
           "language": {
            "search": "Buscar:",
            "pageLength": 10,

          }
    });
  }

  ngOnInit(): void{
      if (!this.cookieService.get("token")) {
          this.router.navigate(['login']);
      }
      this.service.getAllEditLogs().subscribe( response => {

        this.editlogs = response;

      }, (error)=>{
        console.log("ERROR");
      });
  }
  ngAfterViewInit(): void{
    setTimeout(() => {
      this.initTable();
    }, 1000);
  }

}

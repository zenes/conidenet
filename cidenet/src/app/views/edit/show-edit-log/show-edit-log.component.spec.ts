import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowEditLogComponent } from './show-edit-log.component';

describe('ShowEditLogComponent', () => {
  let component: ShowEditLogComponent;
  let fixture: ComponentFixture<ShowEditLogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowEditLogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowEditLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

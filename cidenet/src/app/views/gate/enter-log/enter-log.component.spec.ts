import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterLogComponent } from './enter-log.component';

describe('EnterLogComponent', () => {
  let component: EnterLogComponent;
  let fixture: ComponentFixture<EnterLogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnterLogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

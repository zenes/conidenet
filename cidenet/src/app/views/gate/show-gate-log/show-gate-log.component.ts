import { Component, ViewChild, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { GateLogService } from '../../../core/services/gatelog.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { GateLogI } from '../../../core/interfaces/gatelog.interface'

declare var $;

@Component({
  selector: 'app-show-gate-log',
  templateUrl: './show-gate-log.component.html',
  styleUrls: [
    './show-gate-log.component.css',
   '../../../../../node_modules/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css'
  ]
})
export class ShowGateLogComponent implements AfterViewInit, OnInit {

  constructor(
    private service:GateLogService,
    private router:Router,
    private cookieService: CookieService
  ) { }

  gatelogs:GateLogI[];
  @ViewChild('dataShow') table: ElementRef;
  dataTable: any;
  
  initTable(){
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.dataTable({
      "responsive": true,
      "dom": "<'row'<'col-lg-10 col-md-10 col-xs-12'f><'col-lg-2 col-md-2 col-xs-12'l>>" +
           "<'row'<'col-sm-12'tr>>" +
           "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
           "language": {
            "search": "Buscar:",
          }
    });
  }

  ngOnInit(): void{
      if (!this.cookieService.get("token")) {
          this.router.navigate(['login']);
      }

      this.service.getAllLogs().subscribe( response => {

        this.gatelogs = response;

      }, (error)=>{
        console.log("ERROR");
      });
  }
  ngAfterViewInit(): void{
    setTimeout(() => {
      this.initTable();
    }, 500);
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowGateLogComponent } from './show-gate-log.component';

describe('ShowGateLogComponent', () => {
  let component: ShowGateLogComponent;
  let fixture: ComponentFixture<ShowGateLogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowGateLogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowGateLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

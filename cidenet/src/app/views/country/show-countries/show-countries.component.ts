import { Component, ViewChild, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { CountryService } from '../../../core/services/country.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { CountryI } from '../../../core/interfaces/country.interface'

declare var $;

@Component({
  selector: 'app-show-countries',
  templateUrl: './show-countries.component.html',
  styleUrls: [
    './show-countries.component.css',
   '../../../../../node_modules/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css'
  ]
})
export class ShowCountriesComponent implements AfterViewInit, OnInit {

  constructor(
    private service:CountryService,
    private router:Router,
    private cookieService: CookieService
  ) { }

  countries:CountryI[];
  @ViewChild('dataShow') table: ElementRef;
  dataTable: any;
  
  initTable(){
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.dataTable({
      "responsive": true,
      "dom": "<'row'<'col-lg-10 col-md-10 col-xs-12'f><'col-lg-2 col-md-2 col-xs-12'l>>" +
           "<'row'<'col-sm-12'tr>>" +
           "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
           "language": {
            "search": "Buscar:",
          }
    });
  }

  ngOnInit(): void{

      if (!this.cookieService.get("token")) {
          this.router.navigate(['login']);
      }
      this.service.getAllCountries().subscribe( response => {

        this.countries = response;

      }, (error)=>{
        console.log("ERROR");
      });
  }
  ngAfterViewInit(): void{
    setTimeout(() => {
      this.initTable();
    }, 500);
  }

}

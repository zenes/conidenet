import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from '../../core/services/user.service';
import { UserI } from '../../core/interfaces/user.interface'


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(
    private router:Router,
    private cookieService: CookieService,
    private service:UserService,
  ) { }

  users:UserI[];
  name : any;
  p:number = 1;


  ngOnInit(): void {
      if (!this.cookieService.get("token")) {
          this.router.navigate(['login']);
      }

      this.service.getAllUsers().subscribe( response => {

        this.users = response;

      }, (error)=>{
        console.log("ERROR");
      });
  }
  Search(){
    if(this.name == ""){
        this.ngOnInit();
    }else{
      this.users = this.users.filter(res=>{
        return res.name.toLocaleLowerCase().match(this.name.toLocaleLowerCase());
      })
    }
  }

}

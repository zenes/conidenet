import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
/**
 * Normal components (Login/Dashboard)
 */
import { LoginComponent } from './views/login/login.component'
import { DashboardComponent } from './views/dashboard/dashboard.component'
/**
 * Country components (View,Add,Edit)
 */
import { ShowCountriesComponent } from './views/country/show-countries/show-countries.component'
import { EditCountryComponent } from './views/country/edit-country/edit-country.component'
import { AddCountryComponent } from './views/country/add-country/add-country.component'
/*
 * Branch components (View,Add,Edit)
 */
import { ShowBranchesComponent } from './views/branch/show-branches/show-branches.component'
import { EditBranchComponent } from './views/branch/edit-branch/edit-branch.component'
import { AddBranchComponent } from './views/branch/add-branch/add-branch.component'
/*
 * Gender components (View,Add,Edit)
 */
import { ShowGendersComponent } from './views/gender/show-genders/show-genders.component'
import { EditGenderComponent } from './views/gender/edit-gender/edit-gender.component'
import { AddGenderComponent } from './views/gender/add-gender/add-gender.component'
/*
 * Department components (View,Add,Edit)
 */
import { ShowDepartmentsComponent } from './views/department/show-departments/show-departments.component'
import { EditDepartmentComponent } from './views/department/edit-department/edit-department.component'
import { AddDepartmentComponent } from './views/department/add-department/add-department.component'
/*
 * IdType components (View,Add,Edit)
 */
import { ShowIdTypesComponent } from './views/idtype/show-id-types/show-id-types.component'
import { EditIdTypeComponent } from './views/idtype/edit-id-type/edit-id-type.component'
import { AddIdTypeComponent } from './views/idtype/add-id-type/add-id-type.component'
/*
 * User components (View,Add,Edit)
 */
import { ShowUsersComponent } from './views/user/show-users/show-users.component'
import { EditUserComponent } from './views/user/edit-user/edit-user.component'
import { AddUserComponent } from './views/user/add-user/add-user.component'
/*
 * Gate components (View/Enter)
 * This component adds a new log when the user enters o leaves the office
 */
import { ShowGateLogComponent } from './views/gate/show-gate-log/show-gate-log.component'
import { EnterLogComponent } from './views/gate/enter-log/enter-log.component'
/*
 * Edit Log components (View)
 */
import { ShowEditLogComponent } from './views/edit/show-edit-log/show-edit-log.component'

const routes: Routes = [
  { path:'' , redirectTo:'login' , pathMatch:'full'},
  { path:'login' , component:LoginComponent },
  { path:'dashboard' , component:DashboardComponent },
  // Country
  { path:'countries' , component:ShowCountriesComponent },
  { path:'editCountry' , component:EditCountryComponent },
  { path:'addCountry' , component:AddCountryComponent },
  // Branch
  { path:'branches' , component:ShowBranchesComponent },
  { path:'editBranch' , component:EditBranchComponent },
  { path:'addBranch' , component:AddBranchComponent },
  // Gender
  { path:'genders' , component:ShowGendersComponent },
  { path:'editGender' , component:EditGenderComponent },
  { path:'addGender' , component:AddGenderComponent },
  // Department
  { path:'departments' , component:ShowDepartmentsComponent },
  { path:'editDepartment' , component:EditDepartmentComponent },
  { path:'addDepartment' , component:AddDepartmentComponent },
  // IdType
  { path:'idtypes' , component:ShowIdTypesComponent },
  { path:'editIdType' , component:EditIdTypeComponent },
  { path:'addIdType' , component:AddIdTypeComponent },
  // User
  { path:'users' , component:ShowUsersComponent },
  { path:'editUser/:id' , component:EditUserComponent },
  { path:'addUser' , component:AddUserComponent },
  // GateLog
  { path:'gateLogs' , component:ShowGateLogComponent },
  { path:'enter' , component:EnterLogComponent },
  // EditLogs
  { path:'editLogs' , component:ShowEditLogComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  LoginComponent,
  DashboardComponent,
  ShowCountriesComponent,
  EditCountryComponent,
  AddCountryComponent,
  ShowBranchesComponent,
  EditBranchComponent,
  AddBranchComponent,
  ShowGendersComponent,
  EditGenderComponent,
  AddGenderComponent,
  ShowDepartmentsComponent,
  EditDepartmentComponent,
  AddDepartmentComponent,
  ShowIdTypesComponent,
  EditIdTypeComponent,
  AddIdTypeComponent,
  ShowUsersComponent,
  EditUserComponent,
  AddUserComponent,
  ShowGateLogComponent,
  EnterLogComponent,
  ShowEditLogComponent
]

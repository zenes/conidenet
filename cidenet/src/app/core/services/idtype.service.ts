import { Injectable } from '@angular/core';
import { IdTypeI } from '../interfaces/idtype.interface'
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { CookieService } from 'ngx-cookie-service';

/**
 * This import returns the header and url method for the api
 */
import { setHeaderAuth, url } from './api_config'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IdTypeService {

  constructor(
    private http:HttpClient,
    private cookieService: CookieService
    ) { }

  getAllIdTypes():Observable<IdTypeI[]>{
    let headers = setHeaderAuth(this.cookieService.get("token"))
    let uri = url+"idtypes";

    return this.http.get<IdTypeI[]>(uri, headers);

  }
}

import { HttpHeaders } from '@angular/common/http';

export const url:string = "/api/"
const appSecret:string = "c237b3ef57a19bcdeeab4923df6ae9db1734";

export function setHeaderAuth(token = ""){
  let header = {
      headers : new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${appSecret} ${token}`,
    })
  }
  return header;
}

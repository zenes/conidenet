import { Injectable } from '@angular/core';
import { LoginI } from '../interfaces/login.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

/**
 * This import returns the header and url method for the api
 */
import { setHeaderAuth, url } from './api_config'

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }


  loginByEmail(form:LoginI):Observable<LoginI>{
    let headers = setHeaderAuth()
    let uri = url + "login";
    return this.http.post<LoginI>(uri, form, headers);
  }

}

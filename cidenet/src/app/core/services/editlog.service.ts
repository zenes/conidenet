import { Injectable } from '@angular/core';
import { EditLogI } from '../interfaces/editlog.interface'
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { CookieService } from 'ngx-cookie-service';

/**
 * This import returns the header and url method for the api
 */
import { setHeaderAuth, url } from './api_config'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EditLogService {

  constructor(
    private http:HttpClient,
    private cookieService: CookieService
    ) { }

  getAllEditLogs():Observable<EditLogI[]>{
    let headers = setHeaderAuth(this.cookieService.get("token"))
    let uri = url+"editlogs";

    return this.http.get<EditLogI[]>(uri, headers);

  }
}

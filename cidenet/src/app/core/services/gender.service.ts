import { Injectable } from '@angular/core';
import { GenderI } from '../interfaces/gender.interface'
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { CookieService } from 'ngx-cookie-service';

/**
 * This import returns the header and url method for the api
 */
import { setHeaderAuth, url } from './api_config'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GenderService {

  constructor(
    private http:HttpClient,
    private cookieService: CookieService
    ) { }

  getAllGenders():Observable<GenderI[]>{
    let headers = setHeaderAuth(this.cookieService.get("token"))
    let uri = url+"genders";

    return this.http.get<GenderI[]>(uri, headers);

  }
}

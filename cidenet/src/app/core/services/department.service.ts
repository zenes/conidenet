import { Injectable } from '@angular/core';
import { DepartmentI } from '../interfaces/department.interface'
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { CookieService } from 'ngx-cookie-service';

/**
 * This import returns the header and url method for the api
 */
import { setHeaderAuth, url } from './api_config'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  constructor(
    private http:HttpClient,
    private cookieService: CookieService
    ) { }

  getAllDepartments():Observable<DepartmentI[]>{
    let headers = setHeaderAuth(this.cookieService.get("token"))
    let uri = url+"departments";

    return this.http.get<DepartmentI[]>(uri, headers);

  }
}


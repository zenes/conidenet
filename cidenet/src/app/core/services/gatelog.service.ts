import { Injectable } from '@angular/core';
import { GateLogI } from '../interfaces/gatelog.interface'
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { CookieService } from 'ngx-cookie-service';

/**
 * This import returns the header and url method for the api
 */
import { setHeaderAuth, url } from './api_config'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GateLogService {

  constructor(
    private http:HttpClient,
    private cookieService: CookieService
    ) { }

  getAllLogs():Observable<GateLogI[]>{
    let headers = setHeaderAuth(this.cookieService.get("token"))
    let uri = url+"gatelogs";

    return this.http.get<GateLogI[]>(uri, headers);

  }
}

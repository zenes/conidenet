import { Injectable } from '@angular/core';
import { CountryI } from '../interfaces/country.interface'
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { CookieService } from 'ngx-cookie-service';

/**
 * This import returns the header and url method for the api
 */
import { setHeaderAuth, url } from './api_config'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(
    private http:HttpClient,
    private cookieService: CookieService
    ) { }

  getAllCountries():Observable<CountryI[]>{
    let headers = setHeaderAuth(this.cookieService.get("token"))
    let uri = url+"countries";

    return this.http.get<CountryI[]>(uri, headers);

  }
}

import { Injectable } from '@angular/core';
import { BranchI } from '../interfaces/branch.interface'
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { CookieService } from 'ngx-cookie-service';

/**
 * This import returns the header and url method for the api
 */
import { setHeaderAuth, url } from './api_config'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BranchService {

  constructor(
    private http:HttpClient,
    private cookieService: CookieService
    ) { }

  getAllBranches():Observable<BranchI[]>{
    let headers = setHeaderAuth(this.cookieService.get("token"))
    let uri = url+"branches";

    return this.http.get<BranchI[]>(uri, headers);

  }
}

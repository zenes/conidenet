import { Injectable } from '@angular/core';
import { UserI } from '../interfaces/user.interface'
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { CookieService } from 'ngx-cookie-service';

/**
 * This import returns the header and url method for the api
 */
import { setHeaderAuth, url } from './api_config'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http:HttpClient,
    private cookieService: CookieService
    ) { }

  getAllUsers():Observable<UserI[]>{
    let headers = setHeaderAuth(this.cookieService.get("token"))
    let uri = url+"users";

    return this.http.get<UserI[]>(uri, headers);

  }

  addUser(form:UserI):Observable<UserI>{
    let headers = setHeaderAuth(this.cookieService.get("token"))
    let uri = url + "user";
    return this.http.put<UserI>(uri, form, headers);
  }
}

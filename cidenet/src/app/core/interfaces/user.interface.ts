import { DepartmentI } from './department.interface';
import { IdTypeI } from './idtype.interface';
import { GenderI } from './gender.interface';

export interface UserI{
  id: any;
  idtype_id: any;
  department_id: any;
  gender_id: any;

  employeeid: string;
  documentid: string;
  password: string;
  email: string;
  name: string;
  secondname: string;
  surname: string;
  lastname: string;
  birthday: string;
  startdate: string;
  regdate: string;
  status: string;

  department: DepartmentI;
  document_type: IdTypeI;
  gender: GenderI;

}

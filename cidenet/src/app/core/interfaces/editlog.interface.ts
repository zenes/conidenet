import { UserI } from './user.interface'

export interface EditLogI{
  id: number;
  editor_id: number;
  user_id: number;
  date: string;

  editor: UserI
  user: UserI
}

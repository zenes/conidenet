export interface LoginI{
  id:number;
  document_id:string;
  name:string;
  email:string;
  password:string;
  token:string;
}

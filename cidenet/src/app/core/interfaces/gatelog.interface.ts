export interface GateLogI{
  id: number;
  user_id: number;
  date: string;
  type: string;
}

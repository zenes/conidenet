import { CountryI } from './country.interface';

export interface BranchI{
  id: number;
  country_id: number;
  name: string;
  domain: string;
  url: string;

  country: CountryI;
}

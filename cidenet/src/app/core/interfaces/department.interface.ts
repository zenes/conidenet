import { BranchI } from './branch.interface';

export interface DepartmentI{
  id: number;
  branch_id: number;
  name: string;
  description: string;

  branch: BranchI;
}

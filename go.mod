module gitlab.com/zenes/conidenet

go 1.16

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.1
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
)
